# Renovate Config

These are some [Sharable Renovate Configs] for my personal projects.

## Usage
                                   
Just add the respective `extends` to your [Renovate Config].

### Default Preset (enables all recommended presets)

```json
{
  "extends": [
    "gitlab>hojerst/renovate-config"
  ]
}
```

### Include a specific sub-preset

```json
{
  "extends": [
    "gitlab>hojerst/renovate-config//regex/dockerfile"
  ]
}
```

## Presets

### `comments/dockerfile`

Adds support for renovation of `ENV`/`ARG` values within `Dockerfile`

Just add a marker comment to a variable to renovate this version.

```dockerfile
FROM alpine:latest

# renovate: datasource=github-releases depName=kubernetes/kubernetes
ENV KUBERNETES_VERSION=1.23.0 
```

### `comments/yaml`

Adds support for renovation maps within `.yaml`/`.yml` files.

Just add a marker comment to a key to renovate this version.

```yaml
key:
  # renovate: datasource=github-releases depName=kubernetes/kubernetes
  KUBERNETES_VERSION: 1.23.0
```

### `comments/shell`

Adds support for renovation variable assignments within `.sh`,`.zsh` and `.bash` files.

Just add a marker comment to a variable assignment to renovate this version.

```shell
# renovate: datasource=github-releases depName=kubernetes/kubernetes
export KUBERNETES_VERSION=1.23.0
```
                 

[Sharable Renovate Configs]: https://docs.renovatebot.com/config-presets/
[Renovate Config]: https://docs.renovatebot.com/configuration-options/
